import datetime

import none as none
from django.db import models
from ckeditor.fields import RichTextField
from django_resized import ResizedImageField

# Create your models here.
from django.views.decorators.http import condition


class MainPage(models.Model):
    category = models.ForeignKey('Category', on_delete=models.CASCADE, blank=True, null=True)
    title = RichTextField(null=True, blank=True)
    description = RichTextField(null=True, blank=True)
    image = models.ImageField(upload_to='ManinPage', default='../static/assets/images/bukhara.jpg')

    def __str__(self):
        return str(self.title)


class Category(models.Model):
    name = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='Country', default='.favicon.png')
    title = RichTextField(null=True, blank=True)
    title_tag = RichTextField(null=True, blank=True)
    description = RichTextField(null=True, blank=True)
    about = RichTextField(null=True, blank=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return str(self.name)


class Course(models.Model):
    course = models.FloatField(null=True, blank=True)

    def __str__(self):
        return str(self.course)


class Region(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
    name = models.TextField(null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    price_sum = models.FloatField(null=True, blank=True)
    days = models.IntegerField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    # image = ResizedImageField(size=[1024, 1024], upload_to='Region', default='/favicon.png')
    image = models.ImageField(upload_to='Region', default='/favicon.png')
    title = RichTextField(null=True, blank=True)
    description = RichTextField(null=True, blank=True)

    def __str__(self):
        return str(self.name)


class Service(models.Model):
    name = RichTextField(null=True, blank=True)
    icon = models.ImageField(upload_to='Services', default='/favicon.png')
    icons = models.TextField(null=True, blank=True)
    description = RichTextField(null=True, blank=True)

    def __int__(self):
        return self.name


class Gallery(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
    image = models.ImageField(upload_to='Gallery', default='/favicon.png')
    status = models.BooleanField(default=False)
    description = RichTextField(null=True, blank=True)

    def __int__(self):
        return self.id


class FAQ(models.Model):
    title = RichTextField(null=True, blank=True)
    description = RichTextField(null=True, blank=True)

    def __int__(self):
        return self.title


class Position(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    title = RichTextField(null=True, blank=True)
    description = RichTextField(null=True, blank=True)

    def __str__(self):
        return self.title


class PositionImage(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='PositionImage', default='/favicon.png')
    position = models.ForeignKey(Position, on_delete=models.CASCADE, related_name='position_img')
    description = RichTextField(null=True, blank=True)

    def __int__(self):
        return self.region


class PositionInfo(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    title = RichTextField(null=True, blank=True)
    description = RichTextField(null=True, blank=True)
    image = models.ImageField(upload_to='PositionInfo', default='/favicon.png')

    def __int__(self):
        return self.title


class Transaction(models.Model):
    PROCESSING = 'processing'
    FINISHED = 'finished'
    CANCELED = 'canceled'
    STATUS = ((PROCESSING, PROCESSING), (FINISHED, FINISHED), (CANCELED, CANCELED))
    click_trans_id = models.CharField(max_length=255)
    merchant_trans_id = models.CharField(max_length=255)
    amount = models.CharField(max_length=255)
    action = models.CharField(max_length=255)
    sign_string = models.CharField(max_length=255)
    sign_datetime = models.DateTimeField(max_length=255)
    status = models.CharField(max_length=25, choices=STATUS, default=PROCESSING)

    def __str__(self):
        return self.click_trans_id


class TransactionLog(models.Model):
    log = models.CharField(blank=True, null=True, max_length=2000)
    type = models.CharField(blank=True, null=True, max_length=255)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.log


class Order(models.Model):
    STATUS_CHOICES = (
        ('pending', "Pending"),
        ('accept', "Accept"),
        ('error', 'Payment error')
    )
    PAYMENT_CHOICES = (
        ('click', "Click"),
        ('octo', "Octo"),
        ('payme', 'Payme')
    )
    region = models.ForeignKey(Region, blank=True, null=True, on_delete=models.DO_NOTHING, related_name="orders")
    first_name=models.CharField(blank=True, null=True, max_length=50)
    last_name=models.CharField(blank=True, null=True, max_length=50)
    phone_number=models.CharField(blank=True, null=True, max_length=20)
    comment = models.CharField(default='', max_length=1000, null=True)
    user_count = models.IntegerField(default=1)
    payment_url = models.CharField(default='', blank=True, max_length=1000, null=True)
    total = models.FloatField(blank=True, null=True)
    payment_type = models.CharField(choices=PAYMENT_CHOICES, max_length=512, default="click")
    status = models.CharField(choices=STATUS_CHOICES, default='pending', max_length=50)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    cancel_reason = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        if self.region:
            return self.region.name
        return self.comment
