from modeltranslation import translator
from . import models

@translator.register(models.MainPage)
class IntroduceMEITranslation(translator.TranslationOptions):
    fields = ('title', 'description')

@translator.register(models.Category)
class IntroduceMEITranslation(translator.TranslationOptions):
    fields = ('title', 'title_tag', 'about', 'description')

@translator.register(models.Region)
class IntroduceMEITranslation(translator.TranslationOptions):
    fields = ('title', 'description')

@translator.register(models.Service)
class IntroduceMEITranslation(translator.TranslationOptions):
    fields = ('name', 'description')

@translator.register(models.Gallery)
class IntroduceMEITranslation(translator.TranslationOptions):
    fields = ('description',)

@translator.register(models.FAQ)
class IntroduceMEITranslation(translator.TranslationOptions):
    fields = ('title', 'description')

@translator.register(models.Position)
class IntroduceMEITranslation(translator.TranslationOptions):
    fields = ('title', 'description')

@translator.register(models.PositionImage)
class IntroduceMEITranslation(translator.TranslationOptions):
    fields = ('description',)

@translator.register(models.PositionInfo)
class IntroduceMEITranslation(translator.TranslationOptions):
    fields = ('title', 'description')
    
    