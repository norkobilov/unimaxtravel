# Generated by Django 4.1.3 on 2022-12-16 18:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eratravel', '0011_region_end_date_region_start_date_alter_region_name_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainpage',
            name='image',
            field=models.ImageField(default='../static/assets/images/bukhara.jpg', upload_to='ManinPage'),
        ),
    ]
