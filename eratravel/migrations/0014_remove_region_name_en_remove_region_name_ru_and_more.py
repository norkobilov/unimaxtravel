# Generated by Django 4.1.3 on 2022-12-16 21:09

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eratravel', '0013_mainpage_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='region',
            name='name_en',
        ),
        migrations.RemoveField(
            model_name='region',
            name='name_ru',
        ),
        migrations.RemoveField(
            model_name='region',
            name='name_uz',
        ),
        migrations.AddField(
            model_name='category',
            name='title_tag_en',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='category',
            name='title_tag_ru',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='category',
            name='title_tag_uz',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
    ]
