from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
# Create your views here.
from eratravel.models import Transaction, TransactionLog, Order, Region
from eratravel.click.serializer import ClickUzSerializer
from eratravel.click.click_authorization import click_authorization
from eratravel.click.status import *
from rest_framework.decorators import api_view, permission_classes
from django.http import HttpResponse, JsonResponse
from eratravel.models import *
import json
# import stripe
from django.core.mail import send_mail
from django.conf import settings
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.views import View


# gettext("salom")

def index(request):
    main_info = MainPage.objects.all()
    categories = Category.objects.all()
    galleries = Gallery.objects.filter(status=True).all()
    regions = Region.objects.all()
    services = Service.objects.all()
    context = {
        'main_info': main_info,
        'categories': categories,
        'galleries': galleries,
        'regions': regions,
        'services': services
    }
    return render(request, 'index.html', context)


def country_detail(request, pk):
    region = get_object_or_404(Region, pk=pk)
    positions = Position.objects.filter(region=region)
    course = Course.objects.first()
    context = {
        'region': region,
        'positions': positions,
        'course': course,
    }
    return render(request, 'country_detail.html', context)


def category(request, pk):
    categories = Category.objects.filter(id=pk).get()
    galleries = Gallery.objects.filter(status=True).all()
    regions = Region.objects.filter(category__id=pk).all()
    services = Service.objects.all()
    context = {
        'categories': categories,
        'galleries': galleries,
        'regions': regions,
        'services': services
    }
    return render(request, 'country.html', context)


def country(request):
    categories = Category.objects.all()
    galleries = Gallery.objects.filter(status=True).all()
    regions = Region.objects.all()
    services = Service.objects.all()
    context = {
        'categories': categories,
        'galleries': galleries,
        'regions': regions,
        'services': services
    }
    return render(request, 'country.html', context)


class ClickUzMerchantAPIView(APIView):
    authentication_classes = []
    permission_classes = [AllowAny]

    # http_method_names = ['get', 'head', 'post']
    def post(self, request):
        log = TransactionLog.objects.create(log=request.data)
        log.type = 'card_notify'
        log.save()
        serializer = ClickUzSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        METHODS = {
            PREPARE: self.prepare,
            COMPLETE: self.complete
        }

        merchant_trans_id = serializer.validated_data['merchant_trans_id']
        amount = serializer.validated_data['amount']
        action = serializer.validated_data['action']

        if click_authorization(**serializer.validated_data) is False:
            response_data = {}
            response_data['error'] = AUTHORIZATION_FAIL_CODE
            response_data['message'] = AUTHORIZATION_FAIL
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        if int(merchant_trans_id) >= 5000000:
            response_data = {}
            response_data['error'] = -5
            response_data['error_note'] = "Trans id error"
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        check_order = Order.objects.filter(id=merchant_trans_id).first()
        # check_order = Order.objects.filter(id=merchant_trans_id).first()
        # check_order = Order.objects.filter(id=merchant_trans_id, total=amount, status='payment_waiting').first()
        if not check_order:
            if serializer.validated_data['action'] == "1":
                response_data = {}
                response_data['error'] = -2
                response_data['error_note'] = "Order not found"
            else:
                response_data = {}
                response_data['error'] = ORDER_NOT_FOUND
                response_data['error_note'] = "Order not found"
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        if check_order:
            if check_order.status == "accept":
                response_data = {}
                response_data['error'] = INVALID_ACTION
                response_data['error_note'] = "Invalid action"
                return HttpResponse(json.dumps(response_data), content_type="application/json")
            result = METHODS[action](**serializer.validated_data, response_data=serializer.validated_data)
            return HttpResponse(json.dumps(result), content_type="application/json")

    def prepare(self, click_trans_id: str, merchant_trans_id: str, amount: str, sign_string: str, sign_time: str,
                response_data: dict,
                *args, **kwargs) -> dict:
        """

        :param click_trans_id:
        :param merchant_trans_id:
        :param amount:
        :param sign_string:
        :param response_data:
        :param args:
        :return:
        """
        transaction = Transaction.objects.create(
            click_trans_id=click_trans_id,
            merchant_trans_id=merchant_trans_id,
            amount=amount,
            action=PREPARE,
            sign_string=sign_string,
            sign_datetime=sign_time,
        )
        response_data.update(merchant_prepare_id=transaction.id)
        return response_data

    def complete(self, click_trans_id, amount, error, merchant_prepare_id,
                 response_data, action, *args, **kwargs):
        """

        :param click_trans_id:
        :param merchant_click_trans_id:
        :param amount:
        :param sign_string:
        :param error:
        :param merchant_prepare_id:
        :param response_data:
        :param action:
        :param args:
        :return:
        """
        try:
            transaction = Transaction.objects.get(pk=merchant_prepare_id)

            order = Order.objects.filter(id=transaction.merchant_trans_id).first()

            if error == A_LACK_OF_MONEY:
                response_data.update(error=A_LACK_OF_MONEY_CODE)
                transaction.action = A_LACK_OF_MONEY
                transaction.status = Transaction.CANCELED
                transaction.save()
                return response_data

            if transaction.action == A_LACK_OF_MONEY:
                response_data.update(error=A_LACK_OF_MONEY_CODE)
                return response_data

            if transaction.amount != amount:
                response_data.update(error=INVALID_AMOUNT)
                return response_data

            if transaction.action == action:
                response_data.update(error=INVALID_ACTION)
                return response_data

            transaction.action = action
            transaction.status = Transaction.FINISHED
            transaction.save()
            response_data.update(merchant_confirm_id=transaction.id)
            # self.VALIDATE_CLASS().successfully_payment(transaction.merchant_trans_id, transaction)
            order.status = "accept"
            order.save()

            return response_data
        except Transaction.DoesNotExist:
            response_data.update(error=TRANSACTION_NOT_FOUND)
            return response_data


from django.shortcuts import redirect


@api_view(["POST"])
@permission_classes([AllowAny])
def create_order(request):
    try:
        region_id = request.data["region"]
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')
        phone_number = request.data.get('phone_number')
        payment_type = int(request.data.get('pay_form'))
        region = Region.objects.filter(pk=region_id).first()
        if region:
            if (payment_type == 0):
                order = Order.objects.create(
                    region=region,
                    first_name=first_name,
                    total=region.price_sum,
                    last_name=last_name,
                    phone_number=phone_number,
                    status="pending",
                    payment_type="click"
                )
                order.save()
                order.payment_url = generate_click_url(order.id, region.price_sum, "https://unimaxtravelagency.com/")
                order.save()
                return redirect(order.payment_url)
            else:
                res = {
                    "status": 0,
                    "msg": "Visa card not working at the moment!"
                }
                return JsonResponse(res)
        res = {
            "status": 1,
            "msg": "Order not found"
        }
        return JsonResponse(res)
    except KeyError:
        res = {
            "status": 1,
            "msg": "Order not found"
        }
        return JsonResponse(res)


def generate_click_url(order_id, amount, return_url=None):
    service_id = settings.CLICK_SETTINGS['service_id']
    merchant_id = settings.CLICK_SETTINGS['merchant_id']
    url = f"https://my.click.uz/services/pay?service_id={service_id}&merchant_id={merchant_id}&amount={amount}&transaction_param={order_id}"
    if return_url:
        url += f"&return_url={return_url}"
    return url


# ==============================================================================

# stripe.api_key = settings.STRIPE_SECRET_KEY


class SuccessView(TemplateView):
    template_name = "success.html"


class CancelView(TemplateView):
    template_name = "cancel.html"


class CreateCheckoutSessionView(View):
    def get(self, request):
        regions = Region.objects.all()
        context = {
            'regions': regions,
        }

        return render(request, 'checkout.html', context)

    def post(self, request, *args, **kwargs):
        # product_id = self.kwargs["pk"]
        product = Region.objects.get(id=9)
        YOUR_DOMAIN = "http://127.0.0.1:8000"
        checkout_session = stripe.checkout.Session.create(
            payment_method_types=['card'],
            line_items=[
                {
                    'price_data': {
                        'currency': 'usd',
                        'unit_amount': 50,
                        'product_data': {
                            'name': product.name,
                            # 'images': ['https://i.imgur.com/EHyR2nP.png'],
                        },
                    },
                    'quantity': 1,
                },
            ],
            metadata={
                "product_id": product.id
            },
            mode='payment',
            success_url=YOUR_DOMAIN + '/success/',
            cancel_url=YOUR_DOMAIN + '/cancel/',
        )
        return JsonResponse({
            'id': checkout_session.id
        })
