from django.contrib import admin
from eratravel.models import *
from modeltranslation.admin import TranslationAdmin
from django.utils.translation import gettext_lazy as _
class MyTranslationAdmin(TranslationAdmin):
    class Media:
        js = (
            'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }


# Register your models here.
@admin.register(MainPage)
class IntroduceMEIAdmin(MyTranslationAdmin):
    list_display = ['title', 'description']
    search_fields = ['title', 'description']

@admin.register(Category)
class IntroduceMEIAdmin(MyTranslationAdmin):
    list_display = ['name']
    search_fields = ['name']

@admin.register(Region)
class IntroduceMEIAdmin(MyTranslationAdmin):
    list_display = ['name']
    search_fields = ['name']

@admin.register(Service)
class IntroduceMEIAdmin(MyTranslationAdmin):
    list_display = ['name', 'description']
    search_fields = ['name', 'description']

@admin.register(Gallery)
class IntroduceMEIAdmin(MyTranslationAdmin):
    list_display = ['id', 'description']
    search_fields = ['description']

@admin.register(FAQ)
class IntroduceMEIAdmin(MyTranslationAdmin):
    list_display = ['title', 'description']
    search_fields = ['title', 'description']

@admin.register(Position)
class IntroduceMEIAdmin(MyTranslationAdmin):
    list_display = ['id', 'region', 'title', 'description']
    search_fields = ['title', 'description']

@admin.register(PositionImage)
class IntroduceMEIAdmin(MyTranslationAdmin):
    list_display = ['region', 'image', 'description']
    search_fields = ['description']

@admin.register(PositionInfo)
class IntroduceMEIAdmin(MyTranslationAdmin):
    list_display = ['title', 'description']
    search_fields = ['title', 'description']

admin.site.register(Course)

class OrderAdmin(admin.ModelAdmin):
    list_display = ["region","first_name", "last_name", "total", "payment_type", "status"]

admin.site.register(Order, OrderAdmin)