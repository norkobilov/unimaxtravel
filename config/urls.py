"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from config import settings
from django.views.static import serve
from eratravel.views import (
    create_order, ClickUzMerchantAPIView, index,
    country_detail, country,category, CreateCheckoutSessionView,
    SuccessView,
    CancelView,
    )
urlpatterns = []

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
    path('order', create_order, name='order'),
    path('click', ClickUzMerchantAPIView.as_view()),

    path('home/', index, name='index'),
    path('', index, name='index'),
    path('region/<int:pk>', country, name='country'),
    path('category/<int:pk>', category, name='category'),
    path('country_detail/<int:pk>', country_detail, name='country_detail'),
    path('country/<int:pk>', country, name='country'),

    path('cancel/', CancelView.as_view(), name='cancel'),
    path('success/', SuccessView.as_view(), name='success'),
    path('create-checkout-session', CreateCheckoutSessionView.as_view(), name='create-checkout-session'),
    re_path(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
    re_path(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
)
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


